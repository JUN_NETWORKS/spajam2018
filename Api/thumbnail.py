import requests
import shutil
import urllib

def download_img(url, file_name):
    url_info = urllib.parse.urlparse(url)
    get_param = urllib.parse.parse_qs(url_info.query)
    v = get_param['v'][0]
    url = 'http://i.ytimg.com/vi/{0}/0.jpg'.format(v)

    r = requests.get(url, stream=True)
    if r.status_code == 200:
        with open(file_name, 'wb') as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)

if __name__ == '__main__':
    download_img('https://www.youtube.com/watch?v=dnO0_ZGOJJY', '0.jpg')
