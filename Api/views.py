from django.views import View
from django.shortcuts import render, HttpResponse
from django.http.response import JsonResponse, FileResponse
from django.views.decorators.csrf import csrf_exempt
from ranged_fileresponse import RangedFileResponse
import json
from pytube import YouTube
import ffmpeg
import os
import requests

from .search import Youtube as search
from .thumbnail import download_img

# Create your views here.

thumbnail_url = "http://i.ytimg.com/vi/{0}/0.jpg"


def Search(request):
    search_word = request.GET.get("search", default="ごちうさ")
    Y = search(search_word, result=10)

    search_result = []
    for i in range(10):
        Y_dict = {}
        Y_dict["title"] = Y.title[i]
        Y_dict["id"] = Y.id[i]
        Y_dict["thumbnail"] = thumbnail_url.format(Y.id[i])
        search_result.append(Y_dict)

    return JsonResponse(search_result, safe=False)

# Pytube(Youtube Library)
Y_URL = "https://www.youtube.com/watch?v={}"

@csrf_exempt
def Youtube(request):
    file_name = os.getcwd() + "/audio.wav"
    if request.method == "POST":
        params = json.loads(request.body.decode())
        v_URL = Y_URL.format(params["v"])
        print(params)
    else:
        v_ID = request.GET.get("v", default="SO48dmRMINE")
        v_URL = Y_URL.format(v_ID)

    YouTube(v_URL).streams.filter(only_audio=True).first().download()  # 映像無しmp4をDL
    print("DL終わり")
    y_file = ""
    for file in os.listdir(os.getcwd()):
        if os.path.isfile(file_name):
            os.remove("audio.wav")
        if os.path.splitext(file)[1] == ".mp4":
            stream = ffmpeg.input(file)
            stream = ffmpeg.output(stream, file_name)
            ffmpeg.run(stream)
            y_file = file
            print("変換終わり")
            break

    response = FileResponse(open(file_name, "rb"))
    os.remove(y_file)
    return response


# AiTalk
@csrf_exempt
def Aitalk(request):

    url = "http://webapi.aitalk.jp/webapi/v2/ttsget.php"
    text = request.GET.get("text", default="話しかけられてますよ")
    print(str(request.GET.get("text")))
    data = {
        'username': 'spajam2018',
        'password': 'Lpnsen58',
        'text': str(text),
        'speaker_name': 'nozomi_emo', 'volume': '2.0', 'speed': '1.0', 'pitch': '1.0', 'range': '1.0', 'ext': 'wav'
    }

    req = requests.post(url, params=data)
    print(req)

    saveFileName = '_aitalk.wav'
    with open(saveFileName, 'wb') as f:
        f.write(req.content)

    return FileResponse(open(saveFileName, "rb"))
