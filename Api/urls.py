from django.urls import path
from . import views

app_name = "Api"
urlpatterns = [
    path("Aitalk/", views.Aitalk, name="Aitalk"),
    path("Youtube/", views.Youtube, name="Youtube"),
    path("Search/", views.Search, name="Search"),
]